<?php
require('website_parser.php');
require('fpdf/fpdf.php');

function _urlExist ($url) {
  $a_url = parse_url($url);
  if (!isset($a_url['port']))
    $a_url['port'] = 80;
  $errno = 0;
  $errstr = '';
  $timeout = 30;
  if (isset($a_url['host']) && $a_url['host'] != gethostbyname($a_url['host'])) {
    $fid = fsockopen($a_url['host'], $a_url['port'], $errno, $errstr, $timeout);
    if (!$fid)
      return false;
    $page = isset($a_url['path']) ? $a_url['path'] : '';
    $page .= isset($a_url['query']) ? '?' . $a_url['query'] : '';
    fputs($fid, 'HEAD ' . $page . ' HTTP/1.0' . "\r\n" . 'Host: ' . $a_url['host'] . "\r\n\r\n");
    $head = fread($fid, 4096);
    fclose($fid);
    return preg_match('#^HTTP/.*\s+[200|302]+\s#i', $head);
  } else {
    return false;
  }
}

function _getImagesUrl ($manga, $chapter) {
  $url = 'http://www.mangapanda.com/' . $manga . '/' . $chapter . '/';
  $image = 1;
  $res = array();
  while(_urlExist($url . $image)){
    $parser =  new WebsiteParser($url . $image);
    $parser -> getHrefLinks();
    $foo = $parser -> getImageSources();
    $res[] = $foo[0];
    $image++;
  }
  return $res;    
}

function _downloadImages ($urls, $to){
  $res = array();
  $i = 1;
  foreach ($urls as $url){
    $content = file_get_contents($url);
    if(!is_bool($content))
      file_put_contents($to . $i++ . ".jpg", $content);
  }
}

/* ----------------------------------------------------------------- */

function _imageList ($dir) {
  $listFile = array();
  if ($handle = opendir($dir)) {
    while (false !== ($file = readdir($handle))) {
      if ($file != "." && $file != ".." && $file != ".DS_Store") {
        $listFile[] = $file;
      }
    }
    closedir($handle);
  }
  sort($listFile, SORT_NUMERIC);
  return $listFile;
}

function _makePDf ($src, $save) {
  $pdf = new FPDF();
  $images = _imageList($src);
  foreach ($images as $image) {
    $file = $src . $image;
    $size = getimagesize($file);
    $size[0] = $size[0] * 0.264583333;
    $size[1] = $size[1] * 0.264583333;

    if ($size[0] > $size[1]) {
      $tmp = $size[1];
      $size[1] = $size[0];
      $size[0] = $tmp;
      $pdf->AddPage('L', $size);
    } else {
      $pdf->AddPage('P', $size);
    }
    $pdf->Image($file, 0, 0);
  }
  

  $pdf->Output($save, 'F');
  $pdf->Close();
}

function run ($manga, $chapter, $saveImages, $savePdf){
  $saveImages = $saveImages . $manga . "_" . $chapter . "/";
  if(!is_dir($saveImages))
    mkdir($saveImages);
  _downloadImages(_getImagesUrl($manga, $chapter), $saveImages);
  _makePDf($saveImages, $savePdf . $manga . "_" . $chapter . ".pdf");
}

/* ----------------------------------------------------------------- */

function _getLastDownloadedChapter($manga, $src) {
  $dir = array_diff(scandir($src), array('..', '.'));
  $max = 1;
  foreach($dir as $chapter) {
    if (substr_compare($chapter, $manga . "_", 0, strlen($manga) + 1, true) == 0) {
      $tmp = explode("_", $chapter);
      $c = array_pop($tmp); 
      if($c > $max)
        $max = $c;
    }
  }
  echo $max;
  return $max;
}

function _getLastAvailableChapter($manga, $chapter =  1 ) {
  while (true){
    $content = file_get_contents("http://www.mangapanda.com/$manga/$chapter"); 
    if (preg_match('/' . preg_quote("<h1>404 Not Found</h1>", '/') . '$/', $content)){
      return $chapter;
    }
    $chapter++;
  }
  return -1;
} 

/* ----------------------------------------------------------------- */


?>

